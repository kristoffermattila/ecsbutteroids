package butteroids;

import java.util.HashMap;
import java.util.Map;

public class ComponentBag {
	Map<Integer, ShapeComponent> shapeComponents = new HashMap<>();
	Map<Integer, PositionComponent> positionComponents = new HashMap<>();
}
