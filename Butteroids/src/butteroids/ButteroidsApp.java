package butteroids;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class ButteroidsApp extends Canvas implements Runnable {
    static private final int windowWidth = 640;
    static private final int windowHeight = 480;
    static private final double desiredFps = 60.0;
	static private final double defaultFps = 60.0;
    private boolean running = false;
    private GameEventHandler game;
    JFrame frame;
    public ButteroidsApp() {
		
        setMinimumSize(new Dimension(windowWidth, windowHeight));
        setMaximumSize(new Dimension(windowWidth, windowHeight));
        setPreferredSize(new Dimension(windowWidth, windowHeight));
        frame = new JFrame("Butteroids");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setFocusable(true);
        frame.setFocusTraversalKeysEnabled(false);
        frame.setUndecorated(false);
        frame.setSize(windowWidth, windowHeight);
        frame.setLayout(new BorderLayout());
        frame.add(this, BorderLayout.CENTER);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        game = new GameEventHandler(this);
    }

    public synchronized void start() {
        running = true;
        new Thread(this).start();
    }

    public synchronized void stop() {
        running = false;
		frame.dispose();
    }

    public void render() {
        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, windowWidth, windowHeight);
        g.fillRect(0, 0, windowWidth, windowHeight);
        game.render((Graphics2D) g);
        g.dispose();
        bs.show();
    }

    public void tick(double delta) {
        game.tick(delta);
    }

    @Override
    public void run() {
        long lastTime = System.nanoTime();
        double nsPerTick = 1_000_000_000.0 / desiredFps;
        double delta = 0;
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / nsPerTick;
            lastTime = now;
            boolean shouldRender = false;
            while (delta >= defaultFps/desiredFps) {
                tick(delta);
                delta -= 1.0;
                shouldRender = true;
            }
            if (shouldRender) {
                render();
            }
        }
    }

    public static void main(String[] args) {
        new ButteroidsApp().start();
    }

}
