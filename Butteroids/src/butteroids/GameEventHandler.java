package butteroids;

import java.awt.Graphics2D;

import com.artemis.World;
import com.artemis.WorldConfiguration;
import com.artemis.WorldConfigurationBuilder;

public class GameEventHandler {
    World world;
	SoundPlayer soundplayer;

    public GameEventHandler(ButteroidsApp b) {
		soundplayer = new SoundPlayer();
		WorldConfiguration wc = new WorldConfigurationBuilder().
				with(new LevelSetupSystem(),
					new MovementSystem(),
					new RenderSystem()).build();
		world = new World(wc);
    }

    public void render(Graphics2D g) {
        world.getSystem(RenderSystem.class).render(g);
    }

    public void tick(double delta) {
        world.setDelta((float)delta);
        world.process();
    }
}