package butteroids;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

import com.artemis.Aspect;
import com.artemis.Aspect.Builder;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;

public class LevelSetupSystem extends BaseEntitySystem {
	ComponentMapper<ShapeComponent> sm;
	ComponentMapper<PositionComponent> pm;
	int level;
	
	public LevelSetupSystem() {
		super( Aspect.all() );
	}
	
	public LevelSetupSystem(Builder aspect) {
		super(aspect);
		// TODO Auto-generated constructor stub
	}
	/*
	public LevelSetupSystem(ComponentBag bag) {
		level = 1;
		bag.shapeComponents.put(1, new ShapeComponent(1, createPlayerShape()));
		bag.positionComponents.put(1, new PositionComponent(1, 300, 300));
	}
*/
	@Override
	protected void initialize() {
		level = 1;
		int e = world.create();
		ShapeComponent shape = sm.create(e);
		shape.baseShape = createPlayerShape();
		shape.color = Color.WHITE;
		shape.size = 1;
		PositionComponent p = pm.create(e);
		p.x = 300;
		p.y = 200;
	}
	
	static Path2D createPlayerShape() {
		Path2D path = new Path2D.Double(Path2D.WIND_EVEN_ODD, 3);
        path.moveTo(0, -15);
        path.lineTo(10, 15);
        path.lineTo(-10, 15);
        path.closePath();
        //path.transform(AffineTransform.getTranslateInstance(100, 100));
        return path;
	}
	@Override
	protected void processSystem() {
		// TODO Auto-generated method stub
		
	}
}
