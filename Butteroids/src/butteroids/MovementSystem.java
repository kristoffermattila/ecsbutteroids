package butteroids;

import com.artemis.Aspect;
import com.artemis.Aspect.Builder;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;

public class MovementSystem extends IteratingSystem {
	ComponentMapper<PositionComponent> pm;
    static private final int windowWidth = 640;
    static private final int windowHeight = 480;
	
	public MovementSystem() {
		super( Aspect.all() );
	}
	public MovementSystem(Builder aspect) {
		super(aspect);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void process(int entityId) {
		PositionComponent pos = pm.get(entityId);
		pos.x += 5;
		pos.y += 2;
		
		loopPosition(pos);

	}
	
	private void loopPosition(PositionComponent pos) {
		while(pos.x < 0) {
			pos.x += windowWidth;
		}
		while(pos.x >= windowWidth) {
			pos.x -= windowWidth;
		}
		while(pos.y < 0) {
			pos.y += windowHeight;
		}
		while(pos.y >= windowHeight) {
			pos.y -= windowHeight;
		}
	}

}
