package butteroids;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import com.artemis.Aspect;
import com.artemis.Aspect.Builder;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.utils.IntBag;

public class RenderSystem extends BaseEntitySystem{
	ComponentMapper<ShapeComponent> sm;
	ComponentMapper<PositionComponent> pm;
	Graphics2D g;
	
	public RenderSystem() {
		super( Aspect.all() );
	}
	
	public RenderSystem(Builder aspect) {
		super(aspect);
	}
/*
	public void render(Graphics2D g, ComponentBag bag) {
		bag.shapeComponents.forEach( (id, sc) -> { g.setColor(sc.getColor()); g.fill(sc.getBaseShape()); } );
	}
*/
	public void render(Graphics2D g) {

		IntBag actives = subscription.getEntities();
		int[] ids = actives.getData();
		for (int i = 0, s = actives.size(); s > i; i++) {
			ShapeComponent shape = sm.get(ids[i]);
			PositionComponent pos = pm.get(ids[i]);
			g.setColor(shape.color);
			Shape p = shape.baseShape.createTransformedShape(AffineTransform.getTranslateInstance(pos.x, pos.y));
			g.fill(p);
		}

	}

	@Override
	protected void processSystem() {
		// TODO Auto-generated method stub
		
	}
	
}
