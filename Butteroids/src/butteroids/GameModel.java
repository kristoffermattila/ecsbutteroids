package butteroids;

import java.awt.Graphics2D;

public class GameModel {
	SoundPlayer soundPlayer;
	SystemHandler systemHandler;
	
	GameModel(SoundPlayer soundPlayer) {
		this.soundPlayer = soundPlayer;
		this.systemHandler = new SystemHandler();
	}
	
	void render(Graphics2D g) {
		systemHandler.render(g);
	}
	
	void tick(double delta) {
		
	}
}
