package butteroids;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.artemis.Aspect;
import com.artemis.Aspect.Builder;
import com.artemis.systems.IteratingSystem;

public class InputSystem extends IteratingSystem implements KeyListener {

	public InputSystem() {
		super(Aspect.all(InputComponent.class));
	}
	
	public InputSystem(Builder aspect) {
		super(aspect);
	}

	@Override
	protected void process(int entityId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
